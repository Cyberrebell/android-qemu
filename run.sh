cd "${0%/*}"
qemu-system-x86_64 \
-enable-kvm \
-cpu host \
-smp 8 \
-m 8G \
-device virtio-keyboard-pci \
-serial mon:stdio \
-k de \
-hda android_x86.img
